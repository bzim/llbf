use crate::{compilation::Compilation, exec_env};
use inkwell::{
    basic_block::BasicBlock,
    builder::Builder,
    context::Context,
    execution_engine::ExecutionEngine,
    module::Module,
    types::IntType,
    values::{FunctionValue, IntValue, PointerValue},
    AddressSpace::*,
    IntPredicate,
    OptimizationLevel,
};
use std::{mem, slice::Iter};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Op {
    Inc(u16),
    Dec(u16),
    Next(usize),
    Prev(usize),
    In,
    Out,
    Loop(Vec<Op>),
}

pub struct Executable {
    jit: ExecutionEngine,
    module: Module,
}

struct Compiler<'iter> {
    iter: Iter<'iter, Op>,
    stack: Vec<(Iter<'iter, Op>, BasicBlock, BasicBlock)>,
    module: Module,
    builder: Builder,
    isize_t: IntType,
    i16_t: IntType,
    main_fn: FunctionValue,
    element_fn: FunctionValue,
    resize_front_fn: FunctionValue,
    putchar_fn: FunctionValue,
    getchar_fn: FunctionValue,
    env: PointerValue,
    curr_ptr: PointerValue,
    cell_ptr: PointerValue,
    isize_zero: IntValue,
    i16_zero: IntValue,
    blocks: usize,
}

impl Executable {
    pub fn run(&self) {
        let mut env = exec_env::ExecEnv::new();
        unsafe {
            self.jit
                .get_function::<unsafe extern "C" fn(*mut _)>("bf_main")
                .unwrap()(&mut env as *mut _);
        }
    }

    pub fn render_llvm_source(&self) -> String {
        self.module.print_to_string().to_string()
    }
}

impl Compiler<'iter> {
    fn process(&mut self) -> bool {
        let op = match self.iter.next() {
            Some(op) => op,
            None => {
                return if let Some((iter, start, end)) = self.stack.pop() {
                    self.iter = iter;

                    let curr = self
                        .builder
                        .build_load(&self.curr_ptr, "curr")
                        .into_pointer_value();
                    let contents = self
                        .builder
                        .build_load(&curr, "contents")
                        .into_int_value();
                    let cmp = self.builder.build_int_compare(
                        IntPredicate::EQ,
                        contents,
                        self.i16_zero.clone(),
                        "loop_test",
                    );
                    self.builder.build_conditional_branch(&cmp, &end, &start);

                    self.builder.position_at_end(&end);
                    true
                } else {
                    false
                }
            },
        };

        match op {
            Op::Inc(n) => self.compile_inc(*n),
            Op::Dec(n) => self.compile_dec(*n),
            Op::Next(n) => self.compile_next(*n),
            Op::Prev(n) => self.compile_prev(*n),
            Op::In => self.compile_in(),
            Op::Out => self.compile_out(),
            Op::Loop(vec) => self.compile_loop(vec.iter()),
        }
        true
    }

    fn compile_inc(&mut self, n: u16) {
        let curr = self
            .builder
            .build_load(&self.curr_ptr, "curr")
            .into_pointer_value();
        let contents =
            self.builder.build_load(&curr, "contents").into_int_value();
        let result = self.builder.build_int_add(
            contents,
            self.i16_t.const_int(n as _, false),
            "inc_result",
        );
        self.builder.build_store(&curr, &result);
    }

    fn compile_dec(&mut self, n: u16) {
        let curr = self
            .builder
            .build_load(&self.curr_ptr, "curr")
            .into_pointer_value();
        let contents =
            self.builder.build_load(&curr, "contents").into_int_value();
        let result = self.builder.build_int_sub(
            contents,
            self.i16_t.const_int(n as _, false),
            "dec_result",
        );
        self.builder.build_store(&curr, &result);
    }

    fn compile_next(&mut self, n: usize) {
        let cell = self
            .builder
            .build_load(&self.cell_ptr, "cell")
            .into_int_value();
        let result = self.builder.build_int_add(
            cell,
            self.isize_t.const_int(n as _, false),
            "next_result",
        );
        self.builder.build_store(&self.cell_ptr, &result);

        let new_curr = self
            .builder
            .build_call(
                &self.element_fn,
                &[&self.env, &result],
                "new_curr",
                false,
            ).left()
            .unwrap()
            .into_pointer_value();
        self.builder.build_store(&self.curr_ptr, &new_curr);
    }

    fn compile_prev(&mut self, n: usize) {
        let count = self.isize_t.const_int(n as _, false);
        let cell = self
            .builder
            .build_load(&self.cell_ptr, "cell")
            .into_int_value();
        let cmp = self.builder.build_int_compare(
            IntPredicate::ULT,
            cell.clone(),
            count.clone(),
            "cell_compare",
        );

        let true_block = self
            .main_fn
            .append_basic_block(&format!("sub{}_resizetrue", self.blocks));
        self.blocks += 1;
        let false_block = self
            .main_fn
            .append_basic_block(&format!("sub{}_resizefalse", self.blocks));
        self.blocks += 1;
        let next_block = self
            .main_fn
            .append_basic_block(&format!("sub{}_resizeafter", self.blocks));
        self.blocks += 1;

        self.builder
            .build_conditional_branch(&cmp, &true_block, &false_block);

        self.builder.position_at_end(&true_block);
        let additional = self.builder.build_int_nuw_sub(
            count.clone(),
            cell.clone(),
            "additional",
        );
        self.builder.build_call(
            &self.resize_front_fn,
            &[&self.env, &additional],
            "nil",
            false,
        );

        let new_curr = self
            .builder
            .build_call(
                &self.element_fn,
                &[&self.env, &self.isize_zero],
                "new_curr",
                false,
            ).left()
            .unwrap()
            .into_pointer_value();
        self.builder.build_store(&self.curr_ptr, &new_curr);
        self.builder.build_store(&self.cell_ptr, &self.isize_zero);
        self.builder.build_unconditional_branch(&next_block);

        self.builder.position_at_end(&false_block);
        let result = self.builder.build_int_nuw_sub(cell, count, "prev_result");
        self.builder.build_store(&self.cell_ptr, &result);

        let new_curr = self
            .builder
            .build_call(
                &self.element_fn,
                &[&self.env, &result],
                "new_curr",
                false,
            ).left()
            .unwrap()
            .into_pointer_value();
        self.builder.build_store(&self.curr_ptr, &new_curr);
        self.builder.build_unconditional_branch(&next_block);
        self.builder.position_at_end(&next_block);
    }

    fn compile_in(&mut self) {
        let input = self
            .builder
            .build_call(&self.getchar_fn, &[&self.env], "input", false)
            .left()
            .unwrap();
        let curr = self
            .builder
            .build_load(&self.curr_ptr, "curr")
            .into_pointer_value();
        self.builder.build_store(&curr, &input);
    }

    fn compile_out(&mut self) {
        let curr = self
            .builder
            .build_load(&self.curr_ptr, "curr")
            .into_pointer_value();
        let contents = self.builder.build_load(&curr, "contents");
        self.builder.build_call(
            &self.putchar_fn,
            &[&self.env, &contents],
            "nil",
            false,
        );
    }

    fn compile_loop(&mut self, iter: Iter<'iter, Op>) {
        let start_block = self
            .main_fn
            .append_basic_block(&format!("sub{}_loopstart", self.blocks));
        self.blocks += 1;
        let end_block = self
            .main_fn
            .append_basic_block(&format!("sub{}_loopend", self.blocks));
        self.blocks += 1;

        let curr = self
            .builder
            .build_load(&self.curr_ptr, "curr")
            .into_pointer_value();
        let contents =
            self.builder.build_load(&curr, "contents").into_int_value();
        let cmp = self.builder.build_int_compare(
            IntPredicate::EQ,
            contents,
            self.i16_zero.clone(),
            "loop_test",
        );
        self.builder
            .build_conditional_branch(&cmp, &end_block, &start_block);
        self.builder.position_at_end(&start_block);

        let iter = mem::replace(&mut self.iter, iter);
        self.stack.push((iter, start_block, end_block));
    }
}

impl Compilation<Executable> for [Op] {
    fn compile(&self) -> Executable {
        let context = Context::create();
        let module = context.create_module("brainfuck");
        let builder = context.create_builder();

        let void_t = context.void_type();
        let isize_t =
            context.custom_width_int_type(mem::size_of::<usize>() as u32 * 8);
        let i16_t = context.i16_type();
        let i16_ptr_t = i16_t.ptr_type(Generic);
        let env_t = context.opaque_struct_type("env");
        let env_ptr_t = env_t.ptr_type(Generic);
        let main_t = void_t.fn_type(&[&env_ptr_t], false);
        let element_t = i16_ptr_t.fn_type(&[&env_ptr_t, &isize_t], false);
        let resize_front_t = void_t.fn_type(&[&env_ptr_t, &isize_t], false);
        let putchar_t = void_t.fn_type(&[&env_ptr_t, &i16_t], false);
        let getchar_t = i16_t.fn_type(&[&env_ptr_t], false);

        let main_fn = module.add_function("bf_main", &main_t, None);
        let element_fn = module.add_function("element", &element_t, None);
        let resize_front_fn =
            module.add_function("resize_front", &resize_front_t, None);
        let putchar_fn = module.add_function("putchar", &putchar_t, None);
        let getchar_fn = module.add_function("getchar", &getchar_t, None);

        let block = main_fn.append_basic_block("entry");
        builder.position_at_end(&block);

        let env = main_fn.get_nth_param(0).unwrap().into_pointer_value();
        let curr_ptr = builder.build_alloca(i16_ptr_t.clone(), "curr_ptr");
        let cell_ptr = builder.build_alloca(isize_t.clone(), "cell_ptr");
        let isize_zero = isize_t.const_int(0, false);
        let i16_zero = i16_t.const_int(0, false);
        builder.build_store(&cell_ptr, &isize_zero);
        let init_curr = builder
            .build_call(&element_fn, &[&env, &isize_zero], "init_curr", false)
            .left()
            .unwrap()
            .into_pointer_value();
        builder.build_store(&curr_ptr, &init_curr);

        let mut compiler = Compiler {
            iter: self.iter(),
            stack: Vec::new(),
            blocks: 0,
            module,
            builder,
            isize_t,
            i16_t,
            main_fn,
            element_fn,
            resize_front_fn,
            putchar_fn,
            getchar_fn,
            env,
            curr_ptr,
            cell_ptr,
            isize_zero,
            i16_zero,
        };

        while compiler.process() {}

        compiler.builder.build_return(None);

        let jit = compiler
            .module
            .create_jit_execution_engine(OptimizationLevel::Aggressive)
            .unwrap();
        jit.add_global_mapping(
            &compiler.element_fn,
            exec_env::element as usize,
        );
        jit.add_global_mapping(
            &compiler.resize_front_fn,
            exec_env::resize_front as usize,
        );
        jit.add_global_mapping(
            &compiler.putchar_fn,
            exec_env::putchar as usize,
        );
        jit.add_global_mapping(
            &compiler.getchar_fn,
            exec_env::getchar as usize,
        );
        Executable {
            jit,
            module: compiler.module,
        }
    }
}
