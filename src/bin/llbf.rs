use inkwell::targets::{InitializationConfig, Target};
use llbf::{
    compilation::Compilation,
    hir::{Executable, Op},
    parser::parse,
};
use std::{env::args, fs::File, io::Read, process};

fn print_help() {
    eprintln!("Usage: llbf [options]");
    eprintln!("Options:");
    eprintln!(
        "  <file>, -f <file>, --file <file>         processes <file> as input"
    );
    eprintln!(
        "  -e, --execute                            set execution mode \
         (default)"
    );
    eprintln!(
        "  -l, --llvm-source                        set llvm source generation"
    );
}

fn main() {
    let mut file = None;
    let mut execute = true;
    let mut args = args();
    args.next();

    while let Some(arg) = args.next() {
        match &*arg {
            "-h" | "--help" => {
                print_help();
                process::exit(-1);
            },
            "-f" | "--file" => if let Some(arg) = args.next() {
                if file.is_some() {
                    eprintln!(
                        "Cannot process two files. Please pass only one input."
                    );
                    process::exit(-1);
                }
                file = Some(arg);
            } else {
                eprintln!("Missing value to --file");
                process::exit(-1);
            },
            "-l" | "--llvm-source" => execute = false,
            "-e" | "--execute" => execute = true,
            _ => {
                if file.is_some() {
                    eprintln!(
                        "Cannot process two files. Please pass only one input."
                    );
                    process::exit(-1);
                }
                file = Some(arg);
            },
        }
    }

    let path = match file {
        Some(p) => p,
        _ => {
            eprintln!("error: expected input file");
            process::exit(-1);
        },
    };

    let mut file = match File::open(&path) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("error on {}: {}", path, e);
            process::exit(-1);
        },
    };
    let mut source = Vec::new();
    if let Err(e) = file.read_to_end(&mut source) {
        eprintln!("error on {}: {}", path, e);
        process::exit(-1);
    }

    let ast = match parse(&source) {
        Ok(ast) => ast,
        Err(e) => {
            eprintln!("parse error: {}", e);
            process::exit(-1);
        },
    };

    Target::initialize_native(&InitializationConfig::default()).unwrap();

    let ops: Vec<Op> = ast.compile();
    let exec: Executable = ops.compile();
    if execute {
        exec.run();
    } else {
        println!("{}", exec.render_llvm_source());
    }
}
